import Vue from 'vue';

Vue.prototype.$size = function (base) {
  return {
    'xs': 0,
    'sm': 4,
    'md': 8,
    'lg': 12,
    'xl': 16,
  }[this.$vuetify.breakpoint.name] + (base || 48);
};

Vue.prototype.$asset = {
  achievement: 'https://texture-asset.kirafan.cn/achievement/achievement_{0}',
  advlibraryicon: 'https://texture-asset.kirafan.cn/advlibraryicon/advlibraryicon_{0}',
  advscript: 'https://database-asset.kirafan.cn/advscript/{0}/ADVScriptText_{1}_{2}',
  battleai: 'https://database.kirafan.cn/database/battleai/BattleAIData_{0}',
  characard: 'https://card-asset.kirafan.cn/characard/characard_{0}',
  charaicon: 'https://card-asset.kirafan.cn/charaicon/charaicon_{0}',
  charaillustchara: 'https://illust-asset.kirafan.cn/charaillustchara/charaillust_chara_{0}',
  charaillustfull: 'https://illust-asset.kirafan.cn/charaillustfull/charaillust_full_{0}',
  commonuiatlas: 'https://others-asset.kirafan.cn/uiatlas/commonuiatlas/{0}',
  contentslogo: 'https://texture-asset.kirafan.cn/contenttitlelogo/contentslogo{0}',
  itemicon: 'https://texture-asset.kirafan.cn/itemicon/itemicon_{0}',
  mergedcharaicon: 'https://card-asset.kirafan.cn/mergedcharaicon/charaicon_{0}',
  orbicon: 'https://texture-asset.kirafan.cn/orbicon/orbicon_{0}',
  ordericon: 'https://card-asset.kirafan.cn/ordericon/en_ordericon_{0}',
  originalcharacterillust: 'https://texture-asset.kirafan.cn/originalcharacterillust/originalcharacterillust_{0}',
  originalcharactericon: 'https://texture-asset.kirafan.cn/originalcharactericon/originalcharactericon_{0}',
  roomobjecticon: 'https://texture-asset.kirafan.cn/roomobjecticon/roomobjecticon_{0}_{1}',
  standpic: 'https://standpic-asset.kirafan.cn/{0}/{1}',
  townobjecticon: 'https://texture-asset.kirafan.cn/townobjecticon/townobjecticon_bld_{1}',
  uiatlas: 'https://others-asset.kirafan.cn/uiatlas/{0}',
  voice: 'https://voice-cri.kirafan.cn/Voice_{0}/{1}',
  weaponicon: 'https://texture-asset.kirafan.cn/weaponicon/weaponicon_wpn_{0}',
};

Vue.prototype.$4 = function (x) {
  switch (Math.floor(Math.log10(x))) {
    case 0: return x;
    case 1: return x;
    case 2: return x;
    case 3: return x;
    case 4: return (x / 1e3).toFixed(0) + 'K';
    case 5: return (x / 1e3).toFixed(0) + 'K';
    case 6: return (x / 1e6).toFixed(1) + 'M';
    case 7: return (x / 1e6).toFixed(0) + 'M';
    case 8: return (x / 1e6).toFixed(0) + 'M';
    case 9: return (x / 1e9).toFixed(1) + 'G';
    case 10: return (x / 1e9).toFixed(0) + 'G';
    case 11: return (x / 1e9).toFixed(0) + 'G';
    default:
      if (x > 1e12) return '∞';
      return x;
  }
};

Vue.prototype.$const = {
  sites: {
    calculator: 'https://calc.kirafan.moe/',
    assets: 'https://asset.kirafan.moe/',
    cardmaker: 'https://kirafanautodec.github.io/CharaCardCreater/',

    yarudesu: 'http://kirarafan.com/',
    moegirl: 'https://zh.moegirl.org/%E9%97%AA%E8%80%80%E5%B9%BB%E6%83%B3%E6%9B%B2',
    miraheze: 'https://kirarafantasia.miraheze.org/',
    timer: 'https://icekirby.github.io/kirafan-timer/',

    translations: 'https://gitlab.com/kirafan/translations',
  }
};

Vue.prototype.$isMoe = function () {
  return window.location.host == 'wiki.kirafan.moe';
};

Vue.prototype.$isDev = function () {
  return process.env.NODE_ENV !== 'production';
};

Vue.prototype.$ad = function (i) {
  return i % 20 == 10;
};

Vue.prototype.$zoom = function () {
  if (document.body.clientWidth <= 320) {
    return 0.8;
  }
  if (document.body.clientWidth <= 360 && document.body.clientHeight / document.body.clientWidth < 2) {
    return 0.9;
  }
  return 1.0;
};
